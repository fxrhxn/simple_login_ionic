import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { User } from "../../shared/models/user";
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //User object to store the email and the password.
  user = {} as User;


  constructor(private afAuth: AngularFireAuth, private alertCtrl : AlertController,public navCtrl: NavController, public navParams: NavParams) {

      //Initializing the class variables.
      this.email = ''
      this.password = ''

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  //The function to SIGN UP with email.
  emailSignUp(user : User) {

    //Check if the email and password is undefined.
    if(user.email == undefined || user.password == undefined){

      console.log('UNDEFINED Email or Password')

    }else{

      //Firebase function that creates a new user.
      return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
        .then((user) => {

          //Push to another view page.
          this.navCtrl.push('ViewPage');
          console.log('Authenticated.')

        })
        .catch((error) => console.log(error.toString()) ); //Throw an error just in case.

    }


  }


  //The function to SIGN IN with email.
  emailSignIn(user : User) {

    //Check if email and password is undefined.
    if (user.email == undefined || user.password == undefined) {

        //Give a login fail alert.
        this.loginFailAlert()

    }else{

      //Return a firebase function that signs in the user using email and password.
      return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
        .then((user) => {


          // return this.updateUserData(user); // if using firestore

          //Log the success, and push to the view page.
           console.log('Authenticated')
           this.navCtrl.push('ViewPage');
        })
        .catch((error) => {//Throw an error in case

          //Show an alert that the sign in was unsuccessful.
          this.loginFailAlert()

          //Log the error in the console.
          console.log(error.toString())
        });

    }


  }


  //An alert that is presented showing that the login failed.
  loginFailAlert() {

    //Alert to show that the login failed.
    let alert = this.alertCtrl.create({
      title: 'Failed to Login.',
      subTitle: 'Please try registering, or make sure your email and password are correct.',
      buttons: ['Ok']
    });

    //Present the alert.
    alert.present();
  }




}
